#!/bin/env bash

# PAKETLERİN KURULMASI ####
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
echo "Devam etmek için bir tuşa basın..."
read -n 1 -s
sudo dnf makecache && sudo dnf update --refresh
sudo dnf install \
  @base-x \
  sddm \
  plasma-desktop \
  plasma-discover \
  plasma-systemmonitor \
  sddm-kcm \
  kde-partitionmanager \
  plasma-nm \
  cups \
  kde-print-manager \
  dolphin \
  konsole \
  okular \
  gwenview \
  spectacle \
  kate \
  ark \
  fuse \
  bash-completion \
  dnf-plugins-core \
  tkdnd \
  htop \
  fastfetch \
  bc \
  breeze-gtk \
  sddm-breeze \
  kscreenlocker \
  firewall-config \
  android-tools \
  flatpak \
  virt-manager \
  plymouth \
  firefox \
  gamemode \
  mangohud \
  goverlay \
  steam \
  lutris \
  plymouth-theme-spinner \
  kio-admin \
  filelight \
  --setopt=install_weak_deps=False

echo "Devam etmek için bir tuşa basın..."
read -n 1 -s

## KONFİGÜRASYONLARIN KOPYALANMASI ###
sudo cp -v config/etc/systemd/system/amdgpu-fancontrol.service /etc/systemd/system/
sudo cp -v config/etc/systemd/system/amdgpu-voltage_frequency.service /etc/systemd/system/
sudo cp -v config/etc/gamemode.ini /etc/
sudo cp -v config/usr/bin/amdgpu-fancontrol.sh /usr/bin/
sudo cp -v config/usr/bin/amdgpu-voltage_frequency.sh /usr/bin/
sudo cp -v config/usr/bin/gamemodestart.sh /usr/bin/
sudo cp -v config/usr/bin/gamemodestop.sh /usr/bin/
sudo chmod 444 -v /etc/gamemode.ini
sudo chattr +i /etc/gamemode.ini
sudo chmod 444 -v /etc/systemd/system/amdgpu-voltage_frequency.service
sudo chattr +i /etc/systemd/system/amdgpu-voltage_frequency.service
sudo chmod 444 -v /etc/systemd/system/amdgpu-fancontrol.service
sudo chattr +i /etc/systemd/system/amdgpu-fancontrol.service
sudo chmod 555 -v /usr/bin/amdgpu-fancontrol.sh
sudo chattr +i /usr/bin/amdgpu-fancontrol.sh
sudo chmod 555 -v /usr/bin/amdgpu-voltage_frequency.sh
sudo chattr +i /usr/bin/amdgpu-voltage_frequency.sh
sudo chmod 555 -v /usr/bin/gamemodestart.sh
sudo chattr +i /usr/bin/gamemodestart.sh
sudo chmod 555 -v /usr/bin/gamemodestop.sh
sudo chattr +i /usr/bin/gamemodestop.sh

echo "Devam etmek için bir tuşa basın..."
read -n 1 -s

### KULLANICI İZİNLERİNİN AYARLANMASI ##
sudo touch /etc/sudoers.d/$(whoami)
echo "$(whoami)       ALL=(ALL)       ALL" | sudo tee /etc/sudoers.d/$(whoami)
echo "$(whoami)       ALL=(ALL)       NOPASSWD: /usr/bin/gamemodestart.sh, /usr/bin/gamemodestop.sh" | sudo tee -a /etc/sudoers.d/$(whoami)
sudo sed -i '107s/^/# /' /etc/sudoers
sudo chmod 444 -v /etc/sudoers.d/$(whoami)
sudo chattr +i /etc/sudoers.d/$(whoami)

echo "Devam etmek için bir tuşa basın..."
read -n 1 -s

#### SERVİSLERİN AKTİVE EDİLMESİ #
sudo plymouth-set-default-theme -R "spinner"
sudo systemctl enable amdgpu-fancontrol.service
sudo systemctl enable amdgpu-voltage_frequency.service
sudo systemctl enable sddm.service
sudo systemctl set-default graphical.target