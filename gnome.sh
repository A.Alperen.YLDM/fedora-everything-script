#/bin/bash
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo dnf install -y @base-x gnome-shell gnome-terminal nautilus gnome-browser-connector gnome-tweaks @development-tools gnome-terminal-nautilus xdg-user-dirs xdg-user-dirs-gtk ffmpegthumbnailer gnome-calculator gnome-system-monitor gedit evince file-roller dnf-plugins-core brave-browser
sudo systemctl set-default graphical.target
reboot
