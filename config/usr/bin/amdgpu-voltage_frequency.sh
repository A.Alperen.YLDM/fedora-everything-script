#!/bin/sh
echo "manual" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/power_dpm_force_performance_level 
####GPU_Core-Frequency_Voltage####
echo "s 0 300 750" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 1 588 765" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 2 976 850" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 3 1065 910" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 4 1130 920" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 5 1192 932" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 6 1233 952" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "s 7 1338 1050" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "c" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
####GPU_Memory-Frequency_Voltage####
echo "m 0 300 750" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "m 1 1000 800" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "m 2 1975 880" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
echo "c" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_od_clk_voltage
####SET_GPU-Power-Profile-To-PowerSave####
echo "2" > /sys/devices/pci0000:00/0000:00:03.1/0000:09:00.0/pp_power_profile_mode